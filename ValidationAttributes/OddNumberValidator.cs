using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace MyCustomValidators.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class OddNumberValidator : ValidationAttribute
    {
    
        protected override ValidationResult IsValid(object value, ValidationContext validationContext){

            var model = ( int) validationContext.ObjectInstance;

            if( model % 2 == 0 )
            {
                return new ValidationResult( "odd number." );
            }

            Console.WriteLine( " ----------- > " + model );

            return ValidationResult.Success;
        }

    }
}

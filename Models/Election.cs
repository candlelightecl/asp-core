using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TP2.Models
{
    public class Election{

        [Required(ErrorMessage="Vous devez indiquer votre classe s'il vous plait.")]
        public string candidat{ get; set; }



        public string group{ get; set; }



        [Required(ErrorMessage="Vous devez indiquer votre classe s'il vous plait.")]
        [RegularExpression("^[A-Z]+\\d$", ErrorMessage = "Le nom que vous avez entre n'est pas formel.")]
        public string classe{ get; set; }



        [Required(ErrorMessage="Vous devez indiquer votre classe s'il vous plait.")]
        [RegularExpression("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$", ErrorMessage = "L'email que vous avez entre n'est pas valide.")]
        public string email{ get; set; }




        public List<string> candidat_list { get; set; }
        

        public Election() {
            
            candidat_list = new List<string>();

            candidat_list.Add( "Yassmine1" );
            candidat_list.Add( "Yassmine2" );
            candidat_list.Add( "Yassmine3" );
            candidat_list.Add( "Yassmine4" );
            candidat_list.Add( "Riiiiiim1" );
            candidat_list.Add( "Riiiiiim2" );
            candidat_list.Add( "Riiiiiim3" );
            candidat_list.Add( "Riiiiiim4" );
        }


        public List<string> getListCandidat(){
            return this.candidat_list;
        }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyCustomValidators.ValidationAttributes;

namespace TP2.Models
{
    public class Form2 : IValidatableObject {

        public int id { get; set; }

        [Required]
        public string Text1 { get; set; }

        [RegularExpression("^\\d*[13579]$",ErrorMessage = "Number shoud be odd.")]
        [Required]
        [Range(1, 50)]
        public int Text2 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if( Text2 % 2 == 0 ) 
            {
                yield return new ValidationResult("Number is not odd");
            }
        }
    }

}

 
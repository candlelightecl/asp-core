﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using loginFormMvc.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using TP2.Models;

namespace loginFormMvc.Controllers
{
    public class HomeController : Controller
    {      
        const string SessionKeyMail = "_Mail";
        const string SessionKeyPassword = "_Password";

        const string SessionKeyUser = "_Users";

        public IActionResult Index()
        {
           
            return View(  );
        }

        public IActionResult Connect(){

            // get input email and password from form  
            String email = Request.Form["email"];

            String password = Request.Form["password"];

            // get the users available list and check if the user exist 
            User user = Users.getListOfusers().FirstOrDefault(u=>u.Mail == email && u.Password == password);

            if( user != null ){
            // if the user exist creat a new session and let him access to page 
                HttpContext.Session.SetString( SessionKeyMail, email );
                HttpContext.Session.SetString( SessionKeyPassword, password );
               // unsuported ::  HttpContext.Session.SetString( SessionKeyUser, JsonConvert.ToString( user ) );
               Users.getConnectedUser().Add(user);
               return RedirectToAction("Hello");

            }else{
            // if the user do not exist, redirect him to create a new compte 
                return View("NewUser");
            }
            
        }

        public IActionResult ConnectWithCredentiel(string email, string password){

            // get the users available list and check if the user exist 
            User user = Users.getListOfusers().FirstOrDefault(u=>u.Mail == email && u.Password == password);
         

            if( user != null ){
            // if the user exist creat a new session and let him access to page 
                HttpContext.Session.SetString( SessionKeyMail, email );
                HttpContext.Session.SetString( SessionKeyPassword, password );
               // unsuported ::  HttpContext.Session.SetString( SessionKeyUser, JsonConvert.ToString( user ) );
                Users.getConnectedUser().Add(user);
                return RedirectToAction("Hello" );

            }else{
            // if the user do not exist, redirect him to create a new compte 
                return View("NewUser");
            }
            
        }

        public IActionResult About()
        {
            Election election = new Election(); 
            ViewData["list_candidat"] = election.getListCandidat();
            return View( );
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Hello()
        {
            List<User> list_connected_users = Users.getConnectedUser();
            return View( list_connected_users );
        }

        public IActionResult CreatUser(){
            
            User user = new User () { Mail = Request.Form["email"] , Password = Request.Form["password"] };
            

            if( user != null ){
                Users.getListOfusers().Add( user );
    
                return RedirectToAction( "ConnectWithCredentiel", new { email = user.Mail, password = user.Password } );
            }
            
            return View("NewUser");
            
        }

        public JsonResult getConnectedUser(){
            return Json( Users.getListOfusers() );
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
